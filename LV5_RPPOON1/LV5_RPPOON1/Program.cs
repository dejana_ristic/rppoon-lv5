﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_RPPOON1
{
    class Program
    {
        static void Main(string[] args)
        {
            Product Dress = new Product("Topshop IDOL midi dress", 0.2, 39.05);
            Product Tight = new Product("Nike Running Fast Tight", 0.2, 34.71);

            Box ChosenItems = new Box("Products");
            ChosenItems.Add(Dress);
            ChosenItems.Add(Tight);

            Console.WriteLine("Total product price:" + ChosenItems.Price);
            Console.WriteLine("Total product weight:" + ChosenItems.Weight);
            Console.WriteLine("Product description:" + ChosenItems.Description());

            ShippingService Delivery = new ShippingService(2.99);
            Console.WriteLine("Shipping price for box:" + Delivery.CalculatePrice(ChosenItems));
            Console.WriteLine("Shipping price for Dress only:" + Delivery.CalculatePrice(Dress));
            Console.WriteLine("Shipping price for Tight only:" + Delivery.CalculatePrice(Tight));

            Console.ReadKey();
        }

    }

}
