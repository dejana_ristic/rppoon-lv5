﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON1
{
    class ShippingService
    {
        private double WeightPrice;
        public ShippingService(double WeightPrice)
        {
            this.WeightPrice = WeightPrice;
        }
        public double CalculatePrice(IShipable item)
        {
            return this.WeightPrice * item.Weight;
        }
    }
}
