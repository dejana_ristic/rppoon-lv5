﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON1
{
    class Product:IShipable
    {
        private double price;
        private string description;
        private double weight;

        public Product(string description, double weight, double price)
        {
            this.description = description;
            this.price = price;
            this.weight = weight;
        }
        public double Price { get { return this.price; } }
        public string Description(int depth = 0)
        {
            return new string(' ', depth) + this.description;
        }
        public double Weight { get { return this.weight; } }
    }
}
