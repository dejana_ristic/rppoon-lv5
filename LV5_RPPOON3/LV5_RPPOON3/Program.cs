﻿using System;

namespace LV5_RPPOON3
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lightTheme = new LightTheme();
            DarkTheme darkTheme = new DarkTheme();
            ReminderNote newNote = new ReminderNote("Predati RPPOON do 23h", lightTheme);
            ReminderNote newNote1 = new ReminderNote("Rješiti test KM-PR9", darkTheme);
            newNote.Show();
            newNote1.Show();
            Console.ReadKey();

        }
    }
}
